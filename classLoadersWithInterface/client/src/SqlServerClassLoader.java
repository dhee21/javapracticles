package com.mantiso;

import java.sql.SQLException;

public class SqlServerClassLoader extends ClassLoader {
    private ClassLoader parent;
    private String connectionString;

    SqlServerClassLoader (String connectionString) {

    }
    SqlServerClassLoader (ClassLoader parent, String connectionString) {
        super(parent);
        this.parent = parent;
        this.connectionString = connectionString;
    }


    private byte [] loadClassFromDB (String input) throws SQLException {

        // code to  fetch it from DB;
        byte [] x = new byte[0];
        return x;
    }

    protected Class<?> findClass (String name) throws ClassNotFoundException {
        Class cls = null;
        try{
            //delegate to parent
            cls = parent.loadClass(name);
        } catch (ClassNotFoundException ex) {
            // if not parent can load the class class is ataken from DB and loaded using defineClass method.
            byte [] bytes = new byte[0];
            try {
                bytes = loadClassFromDB(name);
            } catch (SQLException sqlex) {
                throw new ClassNotFoundException("class not found", sqlex);
            }
            return defineClass(name, bytes, 0, bytes.length);
        }
        return cls;
    }

}
