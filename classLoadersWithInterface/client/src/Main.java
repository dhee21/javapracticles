package com.mantiso;

import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import com.pluralsight.IQuote;

public class Main {

    public static void main (String[] args) {
        try {
            URL url = new URL("file:///Ubuld.sers/roposo/Desktop/work/javaPlatform/classLoadersWithInterface/out/artifacts/implementations/implementations.jar");
            URLClassLoader ucl1 = new URLClassLoader(new URL[]{url});
            Class clazz1 = Class.forName("com.pluralsight.Quote", true, ucl1);
            IQuote quote1 = (IQuote) clazz1.newInstance();
            System.out.println(quote1.getQuote());

            URLClassLoader ucl2 = new URLClassLoader(new URL[]{url});
            Class clazz2 = Class.forName("com.pluralsight.Quote", true, ucl2);
            IQuote quote2 = (IQuote) clazz2.newInstance();
            System.out.println(quote2.getQuote());

            System.out.printf("clazz1 == clazz2  %b\n", clazz1 == clazz2);
            System.out.printf("quote1.class == quote2.class  %b\n", quote1.getClass() == quote2.getClass());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        catch (InstantiationException e) {
            e.printStackTrace();
        }
        catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
