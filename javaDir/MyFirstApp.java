import java.util.*;
public class MyFirstApp {
	public static void main (String[] args) {
		LRUCache cache = new LRUCache();
		System.out.println("see the size " + cache.giveSize());

		Scanner scanner = new Scanner(System.in);
		while (true) {
			String x = scanner.nextLine();
			if (x.equals("final")) break;
			String[] arr = x.split(" ");

			if (arr[0].equals("ret")) { 
				System.out.println("trying to retrieve " + arr[1] );
				String retrievedKey = cache.retrieve(arr[1]);
				System.out.println("retrievedinsert " + arr[1]);

			}
			if (arr[0].equals("insert")) { 
				System.out.println("trying to insert " + arr[1]);
				boolean status = cache.insert(arr[1]);
				System.out.println("inserted status" + status);
			}
			if (arr[0].equals("remove")) { 
				System.out.println("trying to remove " + arr[1]);
				boolean status = cache.remove(arr[1]);
				System.out.println("removed " + status);
			}
			cache.seeCache();
			System.out.println("see the size " + cache.giveSize());
		}
	}
}

class LRUCache {
	private int maxCapacity;
	private int totalItems;
	DoublyLinkedList lruLinkedList = new DoublyLinkedList();
	private HashMap<String, Node> cacheContainer = new HashMap<String, Node>();
	LRUCache () {
		maxCapacity = 5;
		totalItems = 0;
	}
	public void seeCache () {
		lruLinkedList.printList();
	}
	public int giveSize () {
		return totalItems;
	}
 	public boolean insert (String key) {

		if (cacheContainer.containsKey(key) == true) {
			return false;
		}

		if (totalItems == maxCapacity) {
			Node headNode = lruLinkedList.getHead();
			String keyToRemove = headNode.getKey();
			cacheContainer.remove(keyToRemove);
			System.out.println("key to remove is due to max Capacity" + keyToRemove);
			lruLinkedList.removeHead();
			totalItems--;
		}

		Node toInsert = new Node(key);
		cacheContainer.put(key, toInsert);
		lruLinkedList.insertAtTail(toInsert);
		totalItems++;
		return true;
	}
	public boolean remove (String key) {
		if (cacheContainer.containsKey(key) != true) {
			return false;
		}
		lruLinkedList.removeNode(cacheContainer.get(key));
		cacheContainer.remove(key);
		totalItems--;
		return true;
	}
	public String retrieve(String key) {
		System.out.println("retrieveing " + key);
		if (cacheContainer.containsKey(key) != true) {
			return null;
		}
		Node nodeToBecomeMostRecentlyUsed = cacheContainer.get(key);
		lruLinkedList.putThisAtEnd(nodeToBecomeMostRecentlyUsed);
		return nodeToBecomeMostRecentlyUsed.getKey();
	}
}

class Node {
	public Node prev, next;
	private String key;
	Node (String val) {
		prev = null;
		key = val;
		next = null;
	}
	public String getKey () {
		return key;
	}
}

class DoublyLinkedList {
	private Node head, tail;
	DoublyLinkedList () {
		head = null;
		tail = null;
	}
	public Node getHead () {
		return head;
	}
	public Node getTail () {
		return tail;
	}
	public void insertAtTail (Node toInsert) {
		System.out.println("in side insertAtTail" + "getHEad = ");
		if (getHead() == null) {
			head = toInsert;
			tail = toInsert;
			return;
		}
		tail.next = toInsert;
		toInsert.prev = tail;
		tail = toInsert;
	}
	public boolean removeNode (Node node) {
		if (node == null) return false;
		if (node.prev == null) {
			return removeHead();
		}
		if (node.next == null) {
			return removeTail();
		}
		node.prev.next = node.next;
		node.next.prev = node.prev;
		node.prev = null;
		node.next = null;
		return false;
	}
	public boolean removeTail () {
		if (tail == null) return false;
		System.out.println("removing " + tail.getKey());
		tail = tail.prev;
		tail.next = null;
		return true;
	}
	public boolean removeHead () {
		if (head == null) {
			return false;
		}
		System.out.println("removing " + head.getKey());
		Node nextHead = head.next;
		head.next = null;
		head = nextHead;
		head.prev = null;
		return true;
	}
	public void putThisAtEnd (Node mruNode) {
		System.out.println("putting this at end ---->" + mruNode.getKey());
		removeNode(mruNode);
		insertAtTail(mruNode);
	}
	public void printList () {
		Node temp = head;
		System.out.println("See the list\n");
		while (temp != null) {
			System.out.println(temp.getKey() + "  ");
			temp = temp.next;
		}
		System.out.println("\n");
	}
}



