package com.pluralsight;

import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;

public class Main {
    public static void main (String[] args) {
        URL url;
        try {
            // here we make url of the jar file stored on classloader/lib
            url = new URL("file:///Users/roposo/Desktop/work/javaPlatform/classLoaders/lib/Quoter.jar");
            // then we define a url using the url. This is just like the classpath for the loader
            // loader will search the class at these given path in array.
            URLClassLoader ucl = new URLClassLoader(new URL[]{url});
            // as loader knows the classpath noe , so we can use the package name of the class
            Class clazz = ucl.loadClass("com.mantiso.Quote");
            // a new instance is made using new instance method.
            // but the new instance goes into Object reference
            // we could have done com.pluralsight.Main but then it defeats the purpose
            // as appClassloader will be run  and no use of making our own classloader.
            // the appclassloader will overwrite the class defined by URL classloader
            // in cache.
            Object o = clazz.newInstance();
            System.out.println(o.toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }
    }
}
// the problem here is that clazz.newinstance is referenced using an Object instance
// so we will not be able to use the methods defined by the loaded class as Object reference
// does not have access to the methods of the loaded class.

// To do that we need to break the Quote down into interfaces and implementations.

// So if i build the code and run
// java -cp out/production/classLoaders com.pluralsight.Main
// hash code of the instance of the Quote class will be printed.
