package com.pluralsight;
import java.net.URL;
import java.net.URLClassLoader;

public class Delegation {
    public static void main (String [] args) {
        // getSystemClassLoader loads UTICLassLoader So we can safely type cast it.
        // this will give the application class loader which is there to load classes in classpath.
        URLClassLoader classLoader = (URLClassLoader) ClassLoader.getSystemClassLoader();

        // here while the classloaders parent is not null we go to parent
        // until we reach the bootstrap classloader where we break out of loop.
        do {
            System.out.println(classLoader);
            // for each classloader we print the urls that classloader searches for
            // loading a class.
            for (URL url : classLoader.getURLs()) {
                System.out.printf("\t %s \n", url.getPath());
            }
        } while ((classLoader = (URLClassLoader) classLoader.getParent()) != null);
        // So it means we have reached the top of the heirarchy , So we print bootstap
        // heirarchy
        System.out.println("Bootstrap classloader");
    }
}

